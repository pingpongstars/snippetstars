from sqlalchemy import (
    Table,
    Column,
    Index,
    Integer,
    DateTime,
    Text,ForeignKey,
    desc, asc,UniqueConstraint
)
from sqlalchemy.orm import relationship, backref

from datetime import datetime

from sqlalchemy.orm import relation

from .meta import Base


from ppss_auth.models import PPSsuser,commonTable

class Snippet(Base,commonTable):
    __tablename__ = 'snippet'
    id = Column(Integer, primary_key=True)
    name = Column(Text)
    snippet = Column(Text)
    privacy = Column(Integer,default=0)
    author_id = Column(Integer, ForeignKey(PPSsuser.id, ondelete="CASCADE"))
    ins_dt = Column(DateTime,default=datetime.now)
    author= relationship(PPSsuser,backref=backref("snippets"))
    tags = relationship("Tag",secondary="snippet_lk_tag",lazy='joined',cascade="all", 
        backref=backref('snippets',lazy='select',order_by='Snippet.ins_dt.desc()'))
    follower = relationship(PPSsuser,secondary="ppssuser_lk_snippet",lazy='joined',cascade="all", 
        backref=backref('following',lazy='select',order_by='Snippet.name'))

class Tag(Base,commonTable):
    __tablename__ = 'tag'
    id = Column(Integer, primary_key=True)
    name = Column(Text)

# snippet tagged with tag
snippetlktag = Table('snippet_lk_tag', Base.metadata,
    Column('snippet_id',Integer,ForeignKey('snippet.id', ondelete="CASCADE"), primary_key=True ),
    Column('tag_id',Integer,ForeignKey('tag.id', ondelete="CASCADE"), primary_key=True)
)


# user 'like' snippet relation
ppssuserlksnippet = Table('ppssuser_lk_snippet', Base.metadata,
    Column('user_id',Integer,ForeignKey(PPSsuser.id, ondelete="CASCADE"), primary_key=True),
    Column('snippet_id',Integer,ForeignKey('snippet.id', ondelete="CASCADE"), primary_key=True )
)




