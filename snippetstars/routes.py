def includeme(config):
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')


    config.add_route('snippet:list',        '/service/snippet/latest')
    config.add_route('snippet:create',        '/service/snippet/create')
    config.add_route('snippet:search',      '/service/snippet/search')
    config.add_route('snippet:like',        '/service/snippet/{id}/like')
    config.add_route('snippet:dislike',     '/service/snippet/{id}/dislike')
    config.add_route('snippet:detail',      '/service/snippet/{id}')
    config.add_route('snippet:detailslug',  '/service/snippet/{id}/{slug}')


    config.add_route('user:status',         '/service/status')
    config.add_route('user:following',      '/service/following')
    config.add_route('user:snippets',       '/service/ownsnippets')