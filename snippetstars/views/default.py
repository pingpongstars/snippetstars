from sqlalchemy.sql.functions import mode
from pyramid.view import view_config
from pyramid.response import Response
from sqlalchemy.exc import SQLAlchemyError

from .. import models


import re
import logging
l = logging.getLogger('ppssauth')

RE_SINGLESPACE = re.compile("(\W)+")


@view_config(route_name='home', renderer='snippetstars:templates/mytemplate.jinja2')
def my_view(request):
    try:
        query = request.dbsession.query(models.MyModel)
        one = query.filter(models.MyModel.name == 'one').one()
    except SQLAlchemyError:
        return Response(db_err_msg, content_type='text/plain', status=500)
    return {'one': one, 'project': 'snippetstars'}


db_err_msg = """\
Pyramid is having a problem using your SQL database.  The problem
might be caused by one of the following things:

1.  You may need to initialize your database tables with `alembic`.
    Check your README.txt for descriptions and try to run it.

2.  Your database server may not be running.  Check that the
    database server referred to by the "sqlalchemy.url" setting in
    your "development.ini" file is running.

After you fix the problem, please restart the Pyramid application to
try it again.
"""

def snippet2map(snippets):
    return {
            "snippets":[
                {'id':s.id
                ,'title':s.name
                ,'author':s.author.username
                ,'date':s.ins_dt.timestamp()
                ,'tags':[{'name':t.name,'id':t.id} for t in s.tags]
                } for s in snippets]        
        }


class User():
    def __init__(self,request) -> None:
        self.request = request
        self.user = None
        if self.request.loggeduser:
            self.user = models.PPSsuser.byId(self.request.loggeduser.id,self.request.dbsession)

    def getUser(self):
        return 

    @view_config(route_name='user:status', renderer='json')
    def userstatus(self):
        if self.user:
            user = self.user
            return{
                "res":True,
                "username" : user.username,
                "userid" : user.id,
                "principals" : user.getPrincipals()
            }
        else:
            return{
                "res":False
            }

    @view_config(route_name='user:following', renderer='json')
    def getFollowed(self):
        if self.user:
            user = self.user
            return {"following":[s.id for s in user.following] }
        return {}

    @view_config(route_name='user:snippets', renderer='json')
    def getSnippets(self):
        limit = int(self.request.params.get("limit",10))
        if self.user:
            user = self.user
            snippets =  self.request.dbsession.query(models.Snippet).filter(models.Snippet.author_id == user.id).limit(limit).all()
            return snippet2map(snippets)
        return {}

class Service():
    def __init__(self,request) -> None:
        self.request = request
        #self.request.response.headers.update({"Access-Control-Allow-Origin" : "*"} )

    @view_config(route_name='snippet:list', renderer='json')
    def latest(self):
        limit = 10
        snippets = self.request.dbsession.query(models.Snippet).limit(limit).all()
        return {
            "snippets":[
                {'id':s.id
                ,'title':s.name
                ,'author':s.author.username
                ,'date':s.ins_dt.timestamp()
                ,'tags':[{'name':t.name,'id':t.id} for t in s.tags]
                } for s in snippets]        
        }

    @view_config(route_name='snippet:search', renderer='json')
    def search(self):
        limit = 10
        name = self.request.params.get("qs","").strip()
        tag = self.request.params.get("tag","")
        author = self.request.params.get("a","").strip()
        query = self.request.dbsession.query(models.Snippet)
        if name:
            query= query.filter(models.Snippet.name.like("%"+re.sub(RE_SINGLESPACE,name.strip(),"%")+"%") )
        if author:
            query = query.filter(models.Snippet.author.username.like("%"+author.strip()+"%"))
        snippets = query.limit(limit).all()
        return {
            "snippets":[
                {'id':s.id
                ,'title':s.name
                ,'author':s.author.username
                ,'date':s.ins_dt.timestamp()
                ,'tags':[{'name':t.name,'id':t.id} for t in s.tags]
                } for s in snippets]        
        }


    @view_config(route_name='snippet:detailslug', renderer='json')
    @view_config(route_name='snippet:detail', renderer='json')
    def detail(self):
        id = int(self.request.matchdict.get("id") )
        snippet = models.Snippet.byId(id,self.request.dbsession)
        if snippet:
            return{
                "id":snippet.id,
                "title":snippet.name,
                "content":snippet.snippet,
                "author":snippet.author.username,
                'tags':[{'name':t.name,'id':t.id} for t in snippet.tags],
                'date':snippet.ins_dt.timestamp()
            }

    @view_config(route_name='snippet:like', renderer='json')
    def likesnippet(self):
        sid = int(self.request.matchdict.get('id'))
        user = models.PPSsuser.byId(self.request.loggeduser.id,self.request.dbsession) if self.request.loggeduser else None
        #self.request.dbsession.add(user)
        snippet = models.Snippet.byId(sid,self.request.dbsession)
        if snippet and user:
            l.info("user and snippet exist")
            for s in user.following:
                if s.id == sid:
                    return{'res':False,"msg":"already following"}
                    l.info("{sid} not found in {slist}".format(sid=sid,slist=[s.id for s in user.following])  )
                
            user.following.append(snippet)
            l.info(f"appending {snippet} to {user.following} of {user}")
            return{'res':True,"msg":"ok"}
        return {'res':False,"msg":"user invalid or snippet not existing"}


    @view_config(route_name='snippet:dislike', renderer='json')
    def dislikesnippet(self):
        sid = int(self.request.matchdict.get('id') )
        user = models.PPSsuser.byId(self.request.loggeduser.id,self.request.dbsession) if self.request.loggeduser else None
        snippet = models.Snippet.byId(sid,self.request.dbsession)
        if snippet and user:
            l.info("user and snippet exist")
            for i,s in enumerate(user.following):
                l.debug(f"checking {sid} against ({s.id},{s.name}")
                if s.id == sid:
                    user.following.pop(i)
                    return{'res':True,"msg":"ok"}    
            l.info("{sid} not found in {slist}".format(sid=sid,slist=[s.id for s in user.following])  )
            return{'res':False,"msg":"not following"}
        return {'res':False,"msg":"user invalid or snippet not existing"}

    
    @view_config(route_name='snippet:create', renderer='json')
    def createsnippet(self):
        user = models.PPSsuser.byId(self.request.loggeduser.id,self.request.dbsession) if self.request.loggeduser else None
        if not user:
            return {"res":False,"msg":"usern not logged"}
        try:
            title = self.request.params.get("title")
            content = self.request.params.get("content")
            tags = list(map (lambda x:x.strip().lower(),  self.request.params.get("tags","").split(",")))
        except:
            return {"res":False,"msg":"required fields are empty"}
        snippet = models.Snippet(
            name = title,
            snippet = content,
            author_id = user.id
        )
        for t in tags:
            tag = self.request.dbsession.query(models.Tag).filter(models.Tag.name == t).first()
            if tag is None:
                tag = models.Tag(name=t)
                self.request.dbsession.add(tag)
            snippet.tags.append(tag) 
        self.request.dbsession.add(snippet)
        return {"res":True,"msg":"Snippet created"}