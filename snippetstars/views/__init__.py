from urllib.parse import urlparse

def cors_tween_factory(handler, registry):
    # one-time configuration code goes here

    def cors_tween(request):
        # code to be executed for each request before
        # the actual application code goes here

        response = handler(request)
        
        parsed_uri = urlparse(request.referer)
        referrer = '{uri.scheme}://{uri.netloc}'.format(uri=parsed_uri)

        #response.headers.update({"Access-Control-Allow-Origin" : domain} )
        response.headers.update({"Access-Control-Allow-Origin" : referrer} )
        response.headers.update({"Access-Control-Allow-Headers": "X-CSRF-Token"})
        response.headers.update({"Access-Control-Allow-Credentials": "true"})
        
        # code to be executed for each request after
        # the actual application code goes here

        return response

    return cors_tween