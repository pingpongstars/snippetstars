import argparse
import sys

from pyramid.paster import bootstrap, setup_logging
from sqlalchemy.exc import OperationalError

from .. import models
from ppss_auth.models import PPSsuser,PPSsgroup


def setup_models(dbsession):
    """
    Add or update models / fixtures in the database.

    """
    usergroup = PPSsgroup.byField('name','signeduser',dbsession)[0]
    user = PPSsuser(username='ppssdev',groups=[usergroup])
    user.setPassword('ppssdev',False)
    dbsession.add(user)
    tags = {
        'dba': models.Tag(name='dba'),
        'db' : models.Tag(name='db'),
        'mysql' : models.Tag(name='mysql'),
        
        'coding' : models.Tag(name='coding'),
        'python' : models.Tag(name='python'),                
        'php' : models.Tag(name='php'),
        'sysadmin' : models.Tag(name='sysadmin'),
        'shell' : models.Tag(name='shell'),

    }
    for k in tags.keys():
        dbsession.add(tags[k])
    
    dbsession.add(
        models.Snippet(
            name="delete all from table",snippet="delete * from <tablename>",
            tags = [tags['dba'],tags['db']]
            ,author=user
        )
    )
    dbsession.add(
        models.Snippet(
            name="create utf8 db",
            snippet="CREATE DATABASE <dbname> CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;",
            tags = [tags['dba'],tags['db'],tags['mysql']]
            ,author=user
        )
    )
    dbsession.add(
        models.Snippet(
            name="create local user",
            snippet="CREATE user '<username>'@'localhost identified by '<userpassword>'",
            tags = [tags['db'],tags['mysql']]
            ,author=user
        )
    )

    dbsession.add(
        models.Snippet(
            name="function on iterable",
            snippet="list(map(lambda a:a*2,myiterable))",
            tags = [tags['python'],tags['coding']]
            ,author=user
        )
    )
    
    dbsession.add(
        models.Snippet(
            name="iterate and change elements",
            snippet="for i,v in enumerate(myiterable):\nmyiterable[i]=v*v",
            tags = [tags['python']]
            ,author=user
        )
    )

def parse_args(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'config_uri',
        help='Configuration file, e.g., development.ini',
    )
    return parser.parse_args(argv[1:])


def main(argv=sys.argv):
    args = parse_args(argv)
    setup_logging(args.config_uri)
    env = bootstrap(args.config_uri)

    try:
        with env['request'].tm:
            dbsession = env['request'].dbsession
            setup_models(dbsession)
            
            


    except OperationalError:
        print('''
Pyramid is having a problem using your SQL database.  The problem
might be caused by one of the following things:

1.  You may need to initialize your database tables with `alembic`.
    Check your README.txt for description and try to run it.

2.  Your database server may not be running.  Check that the
    database server referred to by the "sqlalchemy.url" setting in
    your "development.ini" file is running.
            ''')
